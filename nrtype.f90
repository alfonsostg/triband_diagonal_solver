module nrtype
	integer, parameter :: I4B = SELECTED_INT_KIND(9)
	integer, parameter :: I2B = SELECTED_INT_KIND(4)
	integer, parameter :: I1B = SELECTED_INT_KIND(2)
	integer, parameter :: SP = KIND(1.0)
	integer, parameter :: DP = KIND(1.0D0)
!	integer, parameter :: SPC = KIND((1.0,1.0))
!	integer, parameter :: DPC = KIND((1.0D0,1.0D0))
!	integer, parameter :: LGT = KIND(.true.)
	real(SP), parameter :: PI=3.141592653589793238462643383279502884197_sp
	real(SP), parameter :: PIO2=1.57079632679489661923132169163975144209858_sp
	real(SP), parameter :: TWOPI=6.283185307179586476925286766559005768394_sp
	real(SP), parameter :: SQRT2=1.41421356237309504880168872420969807856967_sp
	real(SP), parameter :: EULER=0.5772156649015328606065120900824024310422_sp
	real(DP), parameter :: PI_D=3.141592653589793238462643383279502884197_dp
	real(DP), parameter :: PIO2_D=1.57079632679489661923132169163975144209858_dp
	real(DP), parameter :: TWOPI_D=6.283185307179586476925286766559005768394_dp
	!TYPE sprs2_sp
	!	INTEGER(I4B) :: n,len
	!	REAL(SP), DIMENSION(:), POINTER :: val
	!	INTEGER(I4B), DIMENSION(:), POINTER :: irow
	!	INTEGER(I4B), DIMENSION(:), POINTER :: jcol
	!END TYPE sprs2_sp
	!TYPE sprs2_dp
	!	INTEGER(I4B) :: n,len
	!	REAL(DP), DIMENSION(:), POINTER :: val
	!	INTEGER(I4B), DIMENSION(:), POINTER :: irow
	!	INTEGER(I4B), DIMENSION(:), POINTER :: jcol
	!END TYPE sprs2_dp
endmodule nrtype
