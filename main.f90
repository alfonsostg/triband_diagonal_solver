program main
    use nrtype
    use TriBandDiagonalSolver

    implicit none

    real(DP), dimension(4,3)                     :: BandMatLOAD
    real(DP), dimension(size(BandMatLOAD,1))     :: bLOAD, X
    integer(I2B)                                 :: i

    BandMatLOAD(1,:)=(/0.0, 1.0 , 2.0 /)  
    BandMatLOAD(2,:)=(/3.5, 1.4, 0.2 /)
    BandMatLOAD(3,:)=(/1.9, -4.3, -2.1 /)
    BandMatLOAD(4,:)=(/3.2, 0.1 , 0.0 /)

    X=0.0

    bLOAD=(/-1.3, 2.5, -0.4, 3.0/)

    call TBSolver(BandMatLOAD, bLOAD, X)


    do i=1,4
    write(6,'(3f6.3, a2, f6.3)') BandMatLOAD(i,:), '|', bLOAD(i) 
    enddo

    write(6,*) 'resultado: '

    write(6,'(4f6.3)') X

endprogram main
