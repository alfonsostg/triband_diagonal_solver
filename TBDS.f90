module TriBandDiagonalSolver
    use nrtype

    implicit none

contains

subroutine TBSolver(BandMat, b, X)
    implicit none

    real(DP), dimension(:,:), intent(in)                        :: BandMat !BandMat=[a,b,c]
    real(DP), dimension(:), intent(in)                          :: b
    real(DP), dimension(size(b)), intent(out)                   :: X
    real(DP), dimension(size(b))                                :: coc
!    real(DP), dimension(size(b,1))                              :: denom
!    real(DP), dimension(size(BandMat,1))
    integer(I2B)                                                :: N, Nb, i, j, hBM, denom


    X=0.0
    hBM=size(BandMat,1)
    N=size(b)
    


    denom=BandMat(1,2)
    X(1)=b(1)/denom

    FrontSub: do j=3,hBM
                coc(j)=BandMat(j-1,3)/denom
                denom=BandMat(j,2)-BandMat(j,1)*coc(j)
                
                X(j)=(b(j)-BandMat(j,1)*X(j-1))/denom
              enddo FrontSub

    Backsub: do j=hBM-2,0,-1
                X(j)=X(j)-coc(j+1)*X(j+1)

             enddo Backsub

    


endsubroutine TBSolver

endmodule TriBandDiagonalSolver
